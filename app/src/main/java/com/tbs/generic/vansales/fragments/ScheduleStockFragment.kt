package com.tbs.generic.vansales.fragments

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Adapters.SerialMapAdapter
import com.tbs.generic.vansales.Adapters.StockItemAdapter
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.NewLoadVanSaleRequest
import com.tbs.generic.vansales.Requests.NonScheduledLoadVanSaleRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.fragment_open_stock.no_data_found
import kotlinx.android.synthetic.main.fragment_open_stock.prgress_bar
import kotlinx.android.synthetic.main.fragment_open_stock.recycleview
import kotlinx.android.synthetic.main.fragment_schedule_stock.*
import kotlinx.android.synthetic.main.include_stock_caluclute.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ScheduleStockFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ScheduleStockFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ScheduleStockFragment : Fragment() {

    private lateinit var preferenceUtils: PreferenceUtils
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var resultListner: ResultListner
    private lateinit var vanStockDetailsDO: VanStockDetailsDO

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment OpenStockFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(vanStockDetailsDO: VanStockDetailsDO, resultListner: ResultListner): ScheduleStockFragment {
            val scheduleStockFragment = ScheduleStockFragment()

            scheduleStockFragment.vanStockDetailsDO = vanStockDetailsDO
            scheduleStockFragment.resultListner = resultListner
            return scheduleStockFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schedule_stock, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferenceUtils = PreferenceUtils(context)
        try {

            setData()
            //loadVehicleStockData()

        } catch (e: Exception) {
            e.localizedMessage
        }

        //Listners
        btn_non_schedule_stock.setOnClickListener({
            listener?.onFragmentNonScheduleStockInteraction()

        })
    }

    private fun setData() {

        calculate()
        val list1 = getSchedueNonScheduleData(vanStockDetailsDO, true)
        var serialListMap = LinkedHashMap<String, ArrayList<SerialListDO>>();
        var requestDos = ArrayList<String>()
        var equipmentDos: ArrayList<SerialListDO> = ArrayList()
        var orderDOS = ArrayList<SerialListDO>()

        if (vanStockDetailsDO != null && vanStockDetailsDO.serialListDOS.size > 0) {
            for (i in vanStockDetailsDO.serialListDOS.indices) {
                if (!requestDos.contains(vanStockDetailsDO.serialListDOS.get(i).equipmentCode)) {
                    requestDos.add(vanStockDetailsDO.serialListDOS.get(i).equipmentCode)
                    equipmentDos.add(vanStockDetailsDO.serialListDOS.get(i))
                }
            }
        }

        if (requestDos.size > 0) {


            for (j in requestDos.indices) {//4
                orderDOS = ArrayList<SerialListDO>()
                for (i in vanStockDetailsDO.serialListDOS.indices) {//10
                    if (vanStockDetailsDO.serialListDOS.get(i).equipmentCode.equals(requestDos.get(j))) {
                        orderDOS.add(vanStockDetailsDO.serialListDOS.get(i))
                    }
                }
                serialListMap.put(requestDos.get(j), orderDOS);
            }
        }

        if (serialListMap.size > 0) {
            var loanReturnAdapter = SerialMapAdapter(activity, serialListMap, equipmentDos)
            recycleview_serials.adapter = loanReturnAdapter
            tvExchange.setVisibility(View.VISIBLE)


        } else {

            recycleview_serials.setVisibility(View.GONE)
            tvExchange.setVisibility(View.GONE)

        }

        val pickUplist = getPickUps(sortList(list1))
        val droplist = getDrops(sortList(list1))


        if (pickUplist.size <= 0 && droplist.size <= 0) {
            showHideViews(false, getString(R.string.no_vehicle_data_found))
        } else {
            showHideViews(true, "")
            if (pickUplist.size > 0) {
                val stockAdapter = StockItemAdapter(activity, pickUplist, "Shipments")
                recycleview_pickups.adapter = stockAdapter
                ll_pickups.visibility = View.VISIBLE
            } else {
                ll_pickups.visibility = View.GONE

            }

            if (droplist.size > 0) {
                val stockAdapter = StockItemAdapter(activity, droplist, "Shipments")
                recycleview_drops.adapter = stockAdapter
                ll_drops.visibility = View.VISIBLE
            } else {
                ll_drops.visibility = View.GONE

            }
        }


    }

    fun getPickUps(list: ArrayList<StockItemDo>): ArrayList<StockItemDo> {

        val pickUpList = ArrayList<StockItemDo>();
        for (item in list) {

            if (item.pickUpOrDrop == 1) {
                pickUpList.add(item)
            }
        }

        return pickUpList;
    }

    fun getDrops(list: ArrayList<StockItemDo>): ArrayList<StockItemDo> {

        val dropList = ArrayList<StockItemDo>();
        for (item in list) {

            if (item.pickUpOrDrop == 2) {
                dropList.add(item)
            }
        }

        return dropList;
    }


    fun sortList(list: ArrayList<StockItemDo>): ArrayList<StockItemDo> {
        Collections.sort(list, { o1: StockItemDo, o2: StockItemDo -> o1.pickUpOrDrop.compareTo(o2.pickUpOrDrop) })
        return list;
    }


    private fun calculate() {
        if (activity != null) {
            tvCapacity.text = "" + vanStockDetailsDO.vehicleCapacity + " " + vanStockDetailsDO.vehicleCapacityUnit
            tvScheduledStock.text = "" + vanStockDetailsDO.totalScheduledStockQty + " " + vanStockDetailsDO.totalScheduledStockUnits
            tvNonScheduledStock.text = "" + vanStockDetailsDO.totalNonScheduledStockQty + " " + vanStockDetailsDO.totalNonScheduledStockUnits

            if (!TextUtils.isEmpty(vanStockDetailsDO.pickUpStock)) {
                llPickUp.visibility = View.VISIBLE
                tvPickUpStock.text = "" + vanStockDetailsDO.pickUpStock + " " + vanStockDetailsDO.vehicleCapacityUnit

            } else {
                llPickUp.visibility = View.GONE
            }
            var scheduleStock = 0.0;
            if (!TextUtils.isEmpty(vanStockDetailsDO.dropStock)) {
                llDrop.visibility = View.VISIBLE
                tvDropStock.text = "" + vanStockDetailsDO.dropStock + " " + vanStockDetailsDO.vehicleCapacityUnit
                scheduleStock = vanStockDetailsDO.dropStock.toDouble()
            } else {
                llDrop.visibility = View.GONE
            }

            var scheduleTotal = 0
            val nonScheduleTotal = vanStockDetailsDO.totalNonScheduledStockQty.toInt()
            //val scheduleStock = vanStockDetailsDO.totalScheduledStockQty.toDouble()
            //  var nonScheduleStock =loadStockMainDo.totalNonScheduledStock

            var total = 0.0.toBigDecimal()

            total = vanStockDetailsDO.vehicleCapacity.toBigDecimal()
            scheduleTotal = scheduleStock.toInt()
//            tvBalanceStock.text = "" + (total - (scheduleTotal + nonScheduleTotal).toBigDecimal()) + " " + vanStockDetailsDO.vehicleCapacityUnit

            var baltotal = (total - (scheduleTotal + nonScheduleTotal).toBigDecimal()).toString()
            tvBalanceStock.text = "" + baltotal + " " + vanStockDetailsDO.vehicleCapacityUnit
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentNonScheduleStockInteraction()
    }

    private fun showHideViews(hasData: Boolean, message: String) {
        try {
            if (hasData) {
                no_data_found.visibility = View.GONE
                ll_main_rcv.visibility = View.VISIBLE
                ll_cal.visibility = View.VISIBLE
            } else {
                no_data_found.visibility = View.VISIBLE
                ll_main_rcv.visibility = View.GONE
                no_data_found.text = message
                ll_cal.visibility = View.GONE
            }
            resultListner.onResultListner(hasData, hasData)
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    fun getSchedueNonScheduleData(vanStockDetails: VanStockDetailsDO, isSchedule: Boolean): ArrayList<StockItemDo> {

        val list = ArrayList<StockItemDo>()

        val stockList = vanStockDetails.stockItemList
        if (isSchedule) {
            val map = HashMap<String, StockItemDo>()
            for (item in stockList) {
                if (item.stockType == 1 && item.pickUpOrDrop == 1) {
                    Log.d("iririririri->", item.pickUpOrDrop.toString())
                    Log.d("itemQty->", item.quantity.toString())
                    if (map.containsKey(item.productName)) {
                        val count = item.quantity.toDouble() + map.getValue(item.productName).updateQty
                        item.updateQty = count
                        map.put(item.productName, item)
                    } else {
                        item.updateQty = item.quantity.toDouble()
                        map.put(item.productName, item)
                    }
                }

            }

            for (mapItem in map) {
                list.add(mapItem.value)
            }

            val stockList1 = vanStockDetails.stockItemList
            if (isSchedule) {
                val map1 = HashMap<String, StockItemDo>()
                for (item1 in stockList1) {
                    if (item1.stockType == 1 && item1.pickUpOrDrop == 2) {
                        Log.d("iririririri->", item1.pickUpOrDrop.toString())
                        Log.d("itemQty->", item1.quantity.toString())
                        if (map1.containsKey(item1.productName)) {
                            val count = item1.quantity.toDouble() + map1.getValue(item1.productName).updateQty
                            item1.updateQty = count
                            map1.put(item1.productName, item1)
                        } else {
                            item1.updateQty = item1.quantity.toDouble()
                            map1.put(item1.productName, item1)
                        }
                    }

                }

                for (mapItem in map1) {
                    list.add(mapItem.value)
                }


            }/* else {

            for (item in stockList) {
                if (item.stockType == 2) {
                    if (map.containsKey(item.productDescription)) {
                        val count = item.quantity.toDouble() + map.getValue(item.productDescription).quantity
                        item.quantity = count
                        map.put(item.productDescription, item)
                    } else {
                        map.put(item.productDescription, item)
                    }
                }

            }
            for (mapItem in map) {
                list.add(mapItem.value);

            }
        }*/
        }
        return list
    }


}
