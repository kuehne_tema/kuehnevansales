package com.tbs.generic.vansales.Activitys

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.tbs.generic.vansales.Adapters.ScheduledProductsAdapter
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_toolbar.*
import kotlinx.android.synthetic.main.shipment_details.*

//
class ShipmentDetailsActivity : BaseActivity() {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var code:String
   var docType = 0

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.shipment_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        disableMenuWithBackButton()
        /* toolbar.setNavigationIcon(R.drawable.back)
         toolbar.setNavigationOnClickListener {
             setResult(14, null)
             finish()
         }
 */

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)
        act_toolbar.setNavigationOnClickListener {
            setResult(14, null)
            finish()
        }

    }

    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        recycleviewLD.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))

        var idd = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        var tvCustomerName = findViewById<View>(R.id.tvCustomerName) as TextView
        var tvReference = findViewById<View>(R.id.tvReference) as TextView
        var tvShipmentNumber = findViewById<View>(R.id.tvShipmentNumber) as TextView

        var dropFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0)

        if (dropFlag != 6) {

            if (idd.length > 0) {
                if (Util.isNetworkAvailable(this)) {
                    val driverListRequest = ActiveDeliveryRequest(dropFlag,idd, this@ShipmentDetailsActivity)
                    driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                        hideLoader()
                        if (isError) {

                            Toast.makeText(this@ShipmentDetailsActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                        } else {
                            var aMonth: String = ""
                            var ayear: String = ""
                            var aDate: String = ""
                            try {
                                var date = activeDeliveryDo.deliveryDate
                                aMonth = date.substring(4, 6)
                                ayear = date.substring(0, 4)
                                aDate = date.substring(Math.max(date.length - 2, 0))
                            } catch (e: Exception) {
                            }
                           code=activeDeliveryDo.customer
                            tvReference.setText(getString(R.string.date) + " : " + aDate + "-" + aMonth + "-" + ayear)
                            tvVRNumber.setText(getString(R.string.vr_id) + "         : " +preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID,""))
                            var loadStockAdapter = ScheduledProductsAdapter(this@ShipmentDetailsActivity, activeDeliveryDo.activeDeliveryDOS, "Shipments")
                            recycleview.setAdapter(loadStockAdapter)
                            if (dropFlag == 2) {
//                                tv_title.setText(getString(R.string.receipt_details_pod))

                                tvCustomerName.setText(getString(R.string.supplier) + " : " + code+" - "+preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))
                                if(activeDeliveryDo.poNumber.isNullOrEmpty()){
                                    tvShipmentNumber.setText(getString(R.string.receipt) + " : " +preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

                                }else{
                                    val request = ActiveDeliveryRequest(1,activeDeliveryDo.loandelivery, this@ShipmentDetailsActivity)
                                    request.setOnResultListener { isError, activeDeliveryDo ->
                                        hideLoader()
                                        if (isError) {

                                        } else {
                                            var loadStockAdapter = ScheduledProductsAdapter(this@ShipmentDetailsActivity,
                                                    activeDeliveryDo.activeDeliveryDOS, "Shipments")
                                            recycleviewLD.setAdapter(loadStockAdapter)
                                        }
                                    }
                                        request.execute()
                                    tvShipmentNumber.setText(getString(R.string.receipt) + " : " +activeDeliveryDo.shipmentNumber)
                                    if(!activeDeliveryDo.poNumber.isNullOrEmpty()){
                                        tvPONumber.setText(getString(R.string.po) + " : " +activeDeliveryDo.poNumber)
                                        tvPONumber.visibility=View.VISIBLE

                                    }
                                    tvLDNumber.visibility=View.VISIBLE
                                    tvLDNumber.setText(getString(R.string.shipment) + " : " +activeDeliveryDo.loandelivery)

                                }

                            } else if (dropFlag == 1) {
//                                tv_title.setText(R.string.shipment_details)

                                tvCustomerName.setText(getString(R.string.customer) + " : " + code+" - "+ preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                                tvShipmentNumber.setText(getString(R.string.shipment) + " : " +  preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

                            } else if (dropFlag == 3) {
//                                tv_title.setText(getString(R.string.retun_details_pod))

                                tvCustomerName.setText(getString(R.string.customer) + " : " + code+" - "+ preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                                tvShipmentNumber.setText(getString(R.string.returns) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

                            } else if (dropFlag == 4) {
//                                tv_title.setText(getString(R.string.picking_ticket_details))

                                tvCustomerName.setText(getString(R.string.customer) + " : " + code+" - "+ preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                                tvShipmentNumber.setText(getString(R.string.picking_tickets) + preferenceUtils.getStringFromPreference(PreferenceUtils.PICK_TICKET, ""))

                            } else if (dropFlag == 5) {
//                                tv_title.setText(getString(R.string.purchase_return_detail))

                                tvCustomerName.setText(getString(R.string.supplier) + " : " + code+" - "+ preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                                tvShipmentNumber.setText(getString(R.string.purchase_return) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

                            } else if (dropFlag == 6) {
                                var pickupDropFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.PICKUP_DROP_FLAG, 0)

//                                tv_title.setText(getString(R.string.misc_stop_details))
                                if (pickupDropFlag == 1) {
                                    tvCustomerName.setText(getString(R.string.customer) + " : " + code+" - "+ preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                                    tvShipmentNumber.setText(getString(R.string.mis_stoo) +preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

                                } else {
                                    tvCustomerName.setText(getString(R.string.supplier) + " : "+ code+" - " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                                    tvShipmentNumber.setText(getString(R.string.mis_stoo) + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

                                }
                                //pickupsupp


                            }else if (dropFlag == 8) {
//                                tv_title.setText(R.string.intersite_det)

                                tvCustomerName.setText(getString(R.string.customer) + " : " + code+" - "+ preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                                tvShipmentNumber.setText(getString(R.string.shipment) + " : "+ preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

                            } else {
//                                tv_title.setText(R.string.shipment_details)

                                tvCustomerName.setText(getString(R.string.customer) + " : " + code+" - "+ preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                                tvShipmentNumber.setText(getString(R.string.shipment) + " : "+  preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

                            }

                        }
                    }
                    driverListRequest.execute()

                } else {
                    showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                }


            } else {

            }
        }else{
            var pickupDropFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.PICKUP_DROP_FLAG, 0)
          var  activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

            if (pickupDropFlag == 1) {
                tvCustomerName.setText(getString(R.string.customer) + " : " + activeDeliverySavedDo.customer+" - "+activeDeliverySavedDo.companyDescription)

                tvShipmentNumber.setText(getString(R.string.mis_stoo) +preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

            } else {
                tvCustomerName.setText(getString(R.string.supplier) + " : "+  activeDeliverySavedDo.customer+" - "+activeDeliverySavedDo.companyDescription)

                tvShipmentNumber.setText(getString(R.string.mis_stoo)  + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

            }
        }

        tv_title.setText(R.string.doc_details)

    }

}