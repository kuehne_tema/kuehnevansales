//package com.tbs.generic.vansales.Activitys
//
//import android.content.Intent
//import androidx.drawerlayout.widget.DrawerLayout
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import android.view.Gravity
//import android.view.View
//import android.view.ViewGroup
//import android.widget.Button
//import android.widget.LinearLayout
//import android.widget.RelativeLayout
//import android.widget.TextView
//import com.tbs.generic.vansales.Adapters.NonBGProductsAdapter
//import com.tbs.generic.vansales.Model.ActiveDeliveryDO
//import com.tbs.generic.vansales.Model.LoadStockDO
//import com.tbs.generic.vansales.R
//import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest
//import com.tbs.generic.vansales.utils.CalendarUtils
//import com.tbs.generic.vansales.utils.PreferenceUtils
//import com.tbs.generic.vansales.utils.Util
//
////
//class NonBGActivity : BaseActivity() {
//    lateinit var loadStockAdapter: NonBGProductsAdapter
//    lateinit var loadStockDOs: ArrayList<LoadStockDO>
//    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
//    lateinit var tvNoDataFound: TextView
////    lateinit var activeDeliveryDos: ArrayList<ActiveDeliveryDO>
//
//    private var isSentForApproval: Boolean = false
//
//    override fun initialize() {
//        val llCategories = layoutInflater.inflate(R.layout.selected_nonbg, null) as RelativeLayout
//        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
//        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
//        changeLocale()
//        toolbar.setNavigationIcon(R.drawable.back)
//        toolbar.setNavigationOnClickListener {
//            showToast(getString(R.string.please_confirm))
//        }
//        tvScreenTitle.text = getString(R.string.non_bg_cylinder_returns)
//
//        initializeControls()
//
//    }
//
//    override fun initializeControls() {
//
//        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
//        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
//
//        val btnConfirm = findViewById<Button>(R.id.btnConfirm)
//        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
//        recycleview.layoutManager = linearLayoutManager
//
//        var id = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
//        var spotId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
//        if (spotId.length > 0) {
//
//            if (Util.isNetworkAvailable(this)) {
//
//                val driverListRequest = ActiveDeliveryRequest(spotId, this@NonBGActivity)
//                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
//                    hideLoader()
//                    if (isError) {
//                        tvNoDataFound.visibility = View.VISIBLE
//                        recycleview.visibility = View.GONE
//                        btnConfirm.visibility = View.GONE
////                showToast(resources.getString(R.string.error_NoData))
//                    } else {
//
//
//                        if (activeDeliveryDo.activeDeliveryDOS.size > 0) {
//                            tvNoDataFound.visibility = View.GONE
//                            recycleview.visibility = View.VISIBLE
//                            btnConfirm.visibility = View.VISIBLE
//                            loadStockAdapter = NonBGProductsAdapter(this@NonBGActivity, activeDeliveryDo.activeDeliveryDOS, "NONBG", "")
//                            recycleview.adapter = loadStockAdapter
//                        } else {
//                            tvNoDataFound.visibility = View.VISIBLE
//                            recycleview.visibility = View.GONE
//                            btnConfirm.visibility = View.GONE
//                        }
//
//
//                    }
//                }
//                driverListRequest.execute()
//            } else {
//                showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)
//
//            }
//
//        } else {
//            if (Util.isNetworkAvailable(this)) {
//                val driverListRequest = ActiveDeliveryRequest(id, this@NonBGActivity)
//                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
//                    hideLoader()
//                    if (isError) {
//                        tvNoDataFound.visibility = View.VISIBLE
//                        recycleview.visibility = View.GONE
//                        btnConfirm.visibility = View.GONE
////                showToast(resources.getString(R.string.error_NoData))
//                    } else {
//
//                        if (activeDeliveryDo.activeDeliveryDOS.size > 0) {
//                            tvNoDataFound.visibility = View.GONE
//                            recycleview.visibility = View.VISIBLE
//                            btnConfirm.visibility = View.VISIBLE
//                            loadStockAdapter = NonBGProductsAdapter(this@NonBGActivity, activeDeliveryDo.activeDeliveryDOS, "NONBG", "")
//                            recycleview.adapter = loadStockAdapter
//                        } else {
//                            tvNoDataFound.visibility = View.VISIBLE
//                            recycleview.visibility = View.GONE
//                            btnConfirm.visibility = View.GONE
//                        }
//
//
//                    }
//                }
//                driverListRequest.execute()
//            } else {
//                showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)
//
//            }
//
//
//        }
//
//
//        btnConfirm.setOnClickListener {
//            Util.preventTwoClick(it)
//
//
//            if (loadStockAdapter != null) {
//                val activeDeliveryDos = loadStockAdapter.getSelectedLoadStockDOs()
//
//
//
//                if (activeDeliveryDos != null && !activeDeliveryDos.isEmpty() && activeDeliveryDos.size > 0) {
//                    var isProductExisted = false
//
//                    for (k in activeDeliveryDos.indices) {
//                        if (activeDeliveryDos.get(k).orderedQuantity > 0) {
//                            isProductExisted = true
//                            break
//                        }
//                    }
//
//                    if (isProductExisted) {
//                        nonBG(activeDeliveryDos)
//                    } else {
//                        val intent = Intent(this@NonBGActivity, SignatureActivity::class.java)
//                        startActivityForResult(intent, 11)
//                    }
//
//
//                } else {
//                    showToast(getString(R.string.no_items_found))
//                }
//            } else {
//                showToast(getString(R.string.no_items_found))
//            }
//
//        }
//    }
//
//    fun nonBG(activeDeliveryDos: ArrayList<ActiveDeliveryDO>) {
//
//
//        var recievedSite = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
//        var vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")
//
//        val customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")
//        val returnDate = CalendarUtils.getDate()
//
//        val siteListRequest = CreateNONBGRequest(recievedSite, customer, vehicleCode, 1, activeDeliveryDos, this@NonBGActivity)
//        siteListRequest.setOnResultListener { isError, loanReturnMainDo ->
//            hideLoader()
//            if (loanReturnMainDo != null) {
//                if (isError) {
//                    showToast("Unable to Create Non Bg")
//                    isSentForApproval=false
//                } else {
//                    if (loanReturnMainDo.status == 20) {
//                        showToast(getString(R.string.updated_successfully))
//
//                        val intent = Intent(this@NonBGActivity, SignatureActivity::class.java)
//                        startActivityForResult(intent, 11)
////                            val intent = Intent()
////                            intent.putExtra("CapturedReturns", true)
////                            setResult(11, intent)
////                            showToast(loanReturnMainDo.message)
////                            preferenceUtils.saveString(PreferenceUtils.NONBG, "NONBG")
////
////                            finish()
//                    } else if (loanReturnMainDo.status == 10) {
//                          isSentForApproval=false
//                        showToast(loanReturnMainDo.message)
//
//                    }
//
//                }
//            } else {
//                showToast("Unable to Create Non Bg")
//            }
//
//        }
//        siteListRequest.execute()
//
//
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == 11 && resultCode == 11) {
//            val intent = Intent()
//            intent.putExtra("CapturedReturns", true)
//            // AppConstants.CapturedReturns= true
//            setResult(11, intent)
//            finish()
//        }
//
//
//    }
//
//    override fun onBackPressed() {
//        if(isSentForApproval){
//            showToast(getString(R.string.please_confirm))
//
//        }
//
//    }
//}