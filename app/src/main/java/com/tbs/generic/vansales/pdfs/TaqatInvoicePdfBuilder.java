package com.tbs.generic.vansales.pdfs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CreateInvoicePaymentDO;
import com.tbs.generic.vansales.Model.PdfInvoiceDo;
import com.tbs.generic.vansales.pdfs.utils.PDFConstants;
import com.tbs.generic.vansales.pdfs.utils.PDFOperations;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.NumberToWord;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by VenuAppasani on 04-11-2018.
 * Copyright (C) 2018 TBS - All Rights Reserved
 */
public class TaqatInvoicePdfBuilder {

    private static Context context;
    private Document document = null;
    private PdfPTable parentTable;

    private Font normalFont, boldFont;

    private TaqatInvoicePdfBuilder() {

    }

    public static TaqatInvoicePdfBuilder getBuilder(Context mContext) {
        //create pdf here
        context = mContext;
        return new TaqatInvoicePdfBuilder();
    }

    private CreateInvoicePaymentDO createInvoicePaymentDO;

    @SuppressLint("StaticFieldLeak")
    public TaqatInvoicePdfBuilder build(CreateInvoicePaymentDO createInvoicePaymentDO) {
        this.createInvoicePaymentDO = createInvoicePaymentDO;
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Generating Payment Please wait...");
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init();
                    addTitleTable();
                    addHeaderLabel();
                    addCustomerDetailsToPdf();
//                    shipmentDetailsToPdf(); using in above method
                    addProductsTableLabels();
                    addProductsToPdf();
                    addFooterToPdf();
                    document.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.cancel();
                sendPDfewDoc();
            }
        }.execute();
        return this;
    }


    private void init() {
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);

        File file = new File(Util.getAppPath(context) + "TaqatInvoice.pdf");
        if (file.exists())
            file.delete();
        try {
            document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("TBS");
            document.addCreator("Venu Appasani");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addHeaderLabel() {
        try {
            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
//            Paragraph headerLabel = new Paragraph(PDFConstants.HEADER_1, headerLabelFont);
            Paragraph headerLabel = new Paragraph(createInvoicePaymentDO.company, headerLabelFont);

            headerLabel.setAlignment(Element.ALIGN_CENTER);
            headerLabel.setPaddingTop(10);
            document.add(headerLabel);
//            Paragraph headerLabel1 = new Paragraph(PDFConstants.HEADER_2, headerLabelFont);

            Paragraph headerLabel1 = new Paragraph(PDFConstants.TAQAT_INVOCE_NAME, headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_CENTER);
            headerLabel1.setPaddingTop(10);
            document.add(headerLabel1);
            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCustomerDetailsToPdf() {
        try {

            PdfPTable headerParentTable = new PdfPTable(1);
            headerParentTable.setPaddingTop(10f);
            headerParentTable.setWidthPercentage(100);

            float[] columnWidths = {3, 0.3f, 5.5f, 2, 0.3f, 2.5f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellOne1 = new PdfPCell();
            cellOne1.setBorder(Rectangle.NO_BORDER);

            PdfPCell cellTwo = new PdfPCell();
            cellTwo.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree = new PdfPCell();
            cellThree.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree1 = new PdfPCell();
            cellThree1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellFour = new PdfPCell();
            cellFour.setBorder(Rectangle.NO_BORDER);


            cellOne.addElement(new Phrase(PDFConstants.INVOICE_NUMBER, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.CUSTOMER_NAME, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.CUSTOMER_TRN, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.CUSTOMER_ADDRESS, boldFont));

            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            cellTwo.addElement(new Phrase(createInvoicePaymentDO.invoiceNumber, normalFont));
            cellTwo.addElement(new Phrase(createInvoicePaymentDO.customerDescription, normalFont));
            cellTwo.addElement(new Phrase("CUSTTRNXXXX", normalFont));
            cellTwo.addElement(new Phrase(getAddress(true), normalFont));

            cellThree.addElement(new Phrase(PDFConstants.DATE, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.TIME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_ID, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_NAME, boldFont));

            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            cellFour.addElement(new Phrase(CalendarUtils.getPDFDate(context), normalFont));
            cellFour.addElement(new Phrase(CalendarUtils.getPDFTime(context), normalFont));
            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, ""), normalFont));
            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""), normalFont));

            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.addElement(headerTable);


            float[] shipmentColumnsWidth = {3, 0.3f, 10};
            PdfPTable shipmentTable = new PdfPTable(shipmentColumnsWidth);
            shipmentTable.setWidthPercentage(100);
            PdfPCell shipCellOne = new PdfPCell();
            shipCellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell shipCellOne1 = new PdfPCell();
            shipCellOne1.setBorder(Rectangle.NO_BORDER);
            PdfPCell shipCellTwo = new PdfPCell();
            shipCellTwo.setBorder(Rectangle.NO_BORDER);


            shipCellOne.addElement(new Phrase(PDFConstants.SUPPLIER_NAME, boldFont));
            shipCellOne.addElement(new Phrase(PDFConstants.SUPPLIER_TRN, boldFont));
            shipCellOne.addElement(new Phrase(PDFConstants.REGISTERED_ADDRESS, boldFont));

            shipCellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            shipCellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            shipCellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            shipCellTwo.addElement(new Phrase(createInvoicePaymentDO.siteDescription, normalFont));
            shipCellTwo.addElement(new Phrase("SUPTRNXXXXX", normalFont));
            shipCellTwo.addElement(new Phrase(getAddress(false), normalFont));
            shipmentTable.addCell(shipCellOne);
            shipmentTable.addCell(shipCellOne1);
            shipmentTable.addCell(shipCellTwo);
            shipmentTable.addCell(getBorderlessCell("", Element.ALIGN_CENTER));

            pdfPCell.addElement(shipmentTable);
            headerParentTable.addCell(pdfPCell);
            document.add(headerParentTable);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void shipmentDetailsToPdf() {

        try {

            float[] shipmentColumnsWidth = {2, 10};
            PdfPTable shipmentTable = new PdfPTable(shipmentColumnsWidth);
            shipmentTable.setWidthPercentage(100);
            PdfPCell shipCellOne = new PdfPCell();
            shipCellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell shipCellTwo = new PdfPCell();
            shipCellTwo.setBorder(Rectangle.NO_BORDER);


            shipCellOne.addElement(new Phrase("Supplier Name", boldFont));
            shipCellOne.addElement(new Phrase("Supplier TRN", boldFont));
            shipCellOne.addElement(new Phrase("Registered Address", boldFont));
            shipCellOne.addElement(new Phrase("  \n"));

            shipCellTwo.addElement(new Phrase(" : " + ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, ""), normalFont));
            shipCellTwo.addElement(new Phrase(" : " + createInvoicePaymentDO.supplierTrn, normalFont));
            shipCellTwo.addElement(new Phrase(" : " + createInvoicePaymentDO.customerStreet + ", " + createInvoicePaymentDO.customerLandMark + ", P.O. Box "
                    + createInvoicePaymentDO.customerPostalCode + ", " + createInvoicePaymentDO.customerCity + ", " + createInvoicePaymentDO.countryname + ".", normalFont));
            shipmentTable.addCell(shipCellOne);
            shipmentTable.addCell(shipCellTwo);

            document.add(shipmentTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addProductsTableLabels() {
        try {

            float[] productColumnsWidth = {2.5f, 10, 3, 4, 4};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            PdfPCell cellFour = new PdfPCell();
            PdfPCell cellFive = new PdfPCell();

            Paragraph pOne = new Paragraph(PDFConstants.SR_NO, boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            Paragraph pTwo = new Paragraph(PDFConstants.PARTICULARS, boldFont);
            pTwo.setAlignment(Element.ALIGN_LEFT);
            cellTwo.addElement(pTwo);

            Paragraph pThree = new Paragraph(PDFConstants.QTY, boldFont);
            pThree.setAlignment(Element.ALIGN_CENTER);
            cellThree.addElement(pThree);

            Paragraph pFour = new Paragraph(PDFConstants.UNIT_PRICE, boldFont);
            pFour.setAlignment(Element.ALIGN_CENTER);
            cellFour.addElement(pFour);

            Paragraph pFive = new Paragraph(PDFConstants.AMOUNT, boldFont);
            pFive.setAlignment(Element.ALIGN_CENTER);
            cellFive.addElement(pFive);


            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);
            productsTable.addCell(cellFour);
            productsTable.addCell(cellFive);

            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsToPdf() {

        try {

            float[] productColumnsWidth = {2.5f, 10, 3, 4, 4};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);

            if (createInvoicePaymentDO.pdfInvoiceDos != null && createInvoicePaymentDO.pdfInvoiceDos.size() > 0) {

                for (int i = 0; i < createInvoicePaymentDO.pdfInvoiceDos.size(); i++) {
                    PdfInvoiceDo pdfInvoiceDo = createInvoicePaymentDO.pdfInvoiceDos.get(i);
                    PdfPCell cellOne = new PdfPCell();
                    PdfPCell cellTwo = new PdfPCell();
                    PdfPCell cellThree = new PdfPCell();
                    PdfPCell cellFour = new PdfPCell();
                    PdfPCell cellFive = new PdfPCell();

                    cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prOne = new Paragraph("" + (i + 1), normalFont);
                    prOne.setAlignment(Element.ALIGN_CENTER);
                    cellOne.addElement(prOne);
                    cellOne.addElement(new Phrase("\n"));
                    cellOne.addElement(new Phrase("\n"));

                    cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cellTwo.addElement(new Phrase("" + pdfInvoiceDo.productDesccription, normalFont));
                    cellTwo.addElement(new Phrase("Discount", normalFont));
                    cellTwo.addElement(new Phrase("VAT  @ " + createInvoicePaymentDO.taxRate + "%", normalFont));

                    cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prThree = new Paragraph("" + pdfInvoiceDo.deliveredQunatity + " " + pdfInvoiceDo.quantityUnits, normalFont);
                    prThree.setAlignment(Element.ALIGN_CENTER);
                    cellThree.addElement(prThree);
                    cellThree.addElement(new Phrase("\n"));
                    cellThree.addElement(new Phrase("\n"));

                    cellFour.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    Paragraph prFour = new Paragraph("" + pdfInvoiceDo.grossPrice, normalFont);
                    prFour.setAlignment(Element.ALIGN_RIGHT);
                    cellFour.addElement(prFour);
                    cellFour.addElement(new Phrase("\n"));
                    cellFour.addElement(new Phrase("\n"));

                    cellFive.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    Paragraph prFive = new Paragraph("" + pdfInvoiceDo.amount, normalFont);
                    prFive.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive);
                    Paragraph prFive1 = new Paragraph("" + pdfInvoiceDo.discount, normalFont);
                    prFive1.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive1);
                    Paragraph prFive2 = new Paragraph("" + pdfInvoiceDo.vatPercentage, normalFont);
                    prFive2.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive2);


                    productsTable.addCell(cellOne);
                    productsTable.addCell(cellTwo);
                    productsTable.addCell(cellThree);
                    productsTable.addCell(cellFour);
                    productsTable.addCell(cellFive);
                }
                document.add(productsTable);

                float[] productColumnsWidth1 = {2.5f, 10, 3, 4, 4};
                PdfPTable totalTable = new PdfPTable(productColumnsWidth1);
                totalTable.setWidthPercentage(100);

                PdfPCell cellOne = new PdfPCell();
                PdfPCell cellTwo = new PdfPCell();
                PdfPCell cellThree = new PdfPCell();
                PdfPCell cellFour = new PdfPCell();
                PdfPCell cellFive = new PdfPCell();

                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));

                cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellTwo.addElement(new Phrase("Total Gross Amount", normalFont));
                cellTwo.addElement(new Phrase("Discount", normalFont));
                cellTwo.addElement(new Phrase("Total Excluding VAT", normalFont));
                cellTwo.addElement(new Phrase("VAT", normalFont));
                cellTwo.addElement(new Phrase("Total", normalFont));

                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase(""));

                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase(""));

                cellFive.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cellFive.addElement(getParagraph("" + /*getTotalGrossAmount(createInvoicePaymentDO.pdfInvoiceDos)*/createInvoicePaymentDO.totalGrossAmount, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.totalDiscount, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.excludingTax, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.totalTax, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.includingTax, Element.ALIGN_RIGHT));

                totalTable.addCell(cellOne);
                totalTable.addCell(cellTwo);
                totalTable.addCell(cellThree);
                totalTable.addCell(cellFour);
                totalTable.addCell(cellFive);

                document.add(totalTable);

                PdfPTable noteTable = new PdfPTable(1);
                noteTable.setWidthPercentage(100);
                double totalAmount = getTotalAmount(createInvoicePaymentDO.pdfInvoiceDos);

                int afterDcimalVal = PDFOperations.getInstance().anyMethod(totalAmount);

                PdfPCell noteCell = new PdfPCell(new Phrase("AED : " + getAmountInLetters(totalAmount) + " and " + afterDcimalVal + "/100 Only-", boldFont));
//                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
                noteCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                noteCell.setPadding(10);
                noteTable.addCell(noteCell);

                document.add(noteTable);
            }
            addEmptySpaceLine(1);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private double getTotalAmount(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalAmount = 0.0f;
        try {
            /*totalAmount = getTotalDiscount(pdfInvoiceDos) -
                    getTotalGrossAmount(pdfInvoiceDos) +
                    getTotalExlTax(pdfInvoiceDos) -
                    getTotalVat(pdfInvoiceDos);*/
            return Double.parseDouble(createInvoicePaymentDO.includingTax);

        } catch (Exception e) {
            return totalAmount;
        }

    }

    private String getAmountInLetters(double amount) {
        return new NumberToWord().convert((int) amount);
        //return NumberToWords.getNumberToWords().getTextFromNumbers(amount);
    }

    private void addEmptySpaceLine(int noOfLines) {
        try {
            PdfPTable emptyTable = new PdfPTable(1);
            emptyTable.setWidthPercentage(100);
            for (int i = 0; i < noOfLines; i++) {
                PdfPCell emptyCell = new PdfPCell();
                emptyCell.setBorder(Rectangle.NO_BORDER);
                emptyCell.addElement(new Paragraph("\n"));
                emptyTable.addCell(emptyCell);
            }
            document.add(emptyTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private Paragraph getParagraph(String content, int alignment) {
        Paragraph p1 = new Paragraph(content, normalFont);
        p1.setAlignment(alignment);
        return p1;
    }

//    private void addSignature() {
//        try {
////
//
//            float[] productColumnsWidth1 = {2, 6};
//            PdfPTable sig = new PdfPTable(productColumnsWidth1);
//            sig.setWidthPercentage(100);
//            Image image = PDFOperations.getInstance().getSignatureFromFile();
//            if(image!=null){
//                image.setBorder(Rectangle.NO_BORDER);
//                PdfPCell pdfPCell = new PdfPCell();
//                pdfPCell.setBorder(Rectangle.NO_BORDER);
//                pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
//                pdfPCell.addElement(image);
//                sig.addCell(pdfPCell);
//                PdfPCell pCell = new PdfPCell();
//                pCell.setBorder(Rectangle.NO_BORDER);
//                sig.addCell(pCell);
//                document.add(sig);
//            }
//
//            PdfPTable custTable = new PdfPTable(1);
//            custTable.setWidthPercentage(100);
//
//
//            PdfPCell pdfPCellSig = new PdfPCell(new Phrase("Customer's Signature:", normalFont));
//            pdfPCellSig.setBorder(Rectangle.NO_BORDER);
//            pdfPCellSig.setHorizontalAlignment(Element.ALIGN_LEFT);
//            custTable.addCell(pdfPCellSig);
//            document.add(custTable);
//
//            addEmptySpaceLine(1);
//
//
//        } catch (BadElementException e) {
//            e.printStackTrace();
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
//    }

    private void addFooterToPdf() {
        try {

//            Paragraph signPara = new Paragraph("                            ");
//            document.add(signPara);//user signature

            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);
            PdfPCell pdfCe = new PdfPCell(new Phrase("THIS IS COMPUTER GENERATED DOCUMENT AND DOES NOT REQUIRE SIGNATURE", boldFont));
            pdfCe.setBorder(Rectangle.NO_BORDER);
            pdfCe.setHorizontalAlignment(Element.ALIGN_LEFT);

            custTable.addCell(pdfCe);

            document.add(custTable);
            document.add(getBorderlessCell("", Element.ALIGN_CENTER));

            float[] regColWidth = {2.2f, 7};
            PdfPTable table = new PdfPTable(regColWidth);
            table.setWidthPercentage(100);

            PdfPCell regCell = new PdfPCell(new Phrase("Registered Office:", boldFont));
            regCell.setHorizontalAlignment(Rectangle.LEFT);
            regCell.setBorder(Rectangle.NO_BORDER);

            PdfPCell regCell2 = new PdfPCell(new Phrase(getAddress(false), normalFont));
            regCell2.setHorizontalAlignment(Rectangle.LEFT);
            regCell2.setBorder(Rectangle.NO_BORDER);

            table.addCell(regCell);
            table.addCell(regCell2);
            document.add(table);

            document.add(getBorderlessCell("", Element.ALIGN_CENTER));

            float[] regAdd = {5, 5};
            PdfPTable regAddTable = new PdfPTable(regAdd);
            regAddTable.setWidthPercentage(100);

            PdfPCell regAddCell = new PdfPCell();
            regAddCell.setHorizontalAlignment(Rectangle.LEFT);
            regAddCell.setBorder(Rectangle.NO_BORDER);
            regAddCell.addElement(new Phrase("T: (+971)" + createInvoicePaymentDO.mobile, normalFont));
            regAddCell.addElement(new Phrase("Email: " + createInvoicePaymentDO.email, normalFont));

            PdfPCell regAddCell2 = new PdfPCell();
            regAddCell2.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            regAddCell2.setBorder(Rectangle.NO_BORDER);
            regAddCell2.addElement(new Phrase("F: +(+971)" + createInvoicePaymentDO.fax, normalFont));
            regAddCell2.addElement(new Phrase("Website: " + createInvoicePaymentDO.website, normalFont));

            regAddTable.addCell(regAddCell);
            regAddTable.addCell(regAddCell2);

            document.add(regAddTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendPDfewDoc() {

        PDFOperations.getInstance().sendpdfMail(context,
                createInvoicePaymentDO.email,
                createInvoicePaymentDO.customerDescription,
                PDFConstants.TAQAT_INVOICE_PDF_NAME, PDFConstants.TAQAT_INVOICE_PDF_NAME);
    }

    private PdfPCell getBorderlessCell(String elementName, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(elementName, normalFont));
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private void addTitleTable() {
        try {
            PdfPTable pdfPTable = new PdfPTable(1);
            pdfPTable.setWidthPercentage(100);
            PdfPCell pdfPCell = new PdfPCell(new Phrase("Taqat Al Ared Owned By Omar Majed Al Alghurair Per Person Company LLC", boldFont));
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPTable.addCell(pdfPCell);
            document.add(pdfPTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAddress(boolean isCustomer) {

        String street, landMark, town, postal, city, countryName;
        if (isCustomer) {
            street = createInvoicePaymentDO.customerStreet;
            landMark = createInvoicePaymentDO.customerLandMark;
            town = createInvoicePaymentDO.customerTown;
            postal = createInvoicePaymentDO.customerPostalCode;
            city = createInvoicePaymentDO.customerCity;
            countryName = createInvoicePaymentDO.countryname;
        } else {
            street = createInvoicePaymentDO.siteStreet;
            landMark = createInvoicePaymentDO.siteLandMark;
            town = createInvoicePaymentDO.siteTown;
            city = createInvoicePaymentDO.siteCity;
            postal = "";
            countryName = createInvoicePaymentDO.siteCountry;
        }
        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName;
        }

        return finalString;
    }
}
