package com.tbs.generic.vansales.DirectionActivitys;


import com.google.android.gms.maps.model.LatLng;

public class Segment {

    private LatLng start;

    private String instruction;

    private int length;

    private double distance;
    

    private String maneuver;



    public Segment() {
    }




    public void setInstruction(final String turn) {
        this.instruction = turn;
    }



    public String getInstruction() {
        return instruction;
    }


    public void setPoint(final LatLng point) {
        start = point;
    }


    public LatLng startPoint() {
        return start;
    }



    public Segment copy() {
        final Segment copy = new Segment();
        copy.start = start;
        copy.instruction = instruction;
        copy.length = length;
        copy.distance = distance;
        copy.maneuver = maneuver;
        return copy;
    }


    public void setLength(final int length) {
        this.length = length;
    }


    public int getLength() {
        return length;
    }


    public void setDistance(double distance) {
        this.distance = distance;
    }


    public double getDistance() {
        return distance;
    }

    public void setManeuver(String man) {
        maneuver = man;
    }
    
    public String getManeuver() {
        return maneuver;
    }
}
