package com.tbs.generic.vansales.Model;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class ReportMainDo implements Serializable {


    public String type = "";
    public String contractNumber = "";
    public String preparationNumber = "";
    public String lineNumber = "";

    public String contractReference = "";
    public String bookingReference = "";

    public String companyId = "";
    public String companyName = "";
    public String companyAdd1 = "";
    public String companyAdd2 = "";
    public String companyAdd3 = "";
    public String companyCity = "";
    public String companyPostalCode = "";
    public String companyCountry = "";

    public String customer = "";
    public String customerName = "";
    public String customerAdd1 = "";
    public String customerAdd2 = "";
    public String customerAdd3 = "";
    public String customerCity = "";
    public String customerPostalCode = "";
    public String customerCountry = "";

    public String inspectionLocation = "";
    public String inspectionAdd1 = "";
    public String inspectionAdd2 = "";
    public String inspectionAdd3 = "";
    public String inspectionCity = "";
    public String inspectionPostalCode = "";
    public String inspectionCountry = "";

    public String product = "";
    public String productDescription = "";
    public String serialNumber = "";
    public String signature = "";

    public String rentalStartDate = "";
    public String rentalEndDate = "";

    public String user = "";
    public String date = "";
    public String time = "";

    public String conductedBy = "";
    public String mainComment = "";
    public int flag = 0;
    public String message = "";
    public int serviceOrRepair = 0;


    public ArrayList<ReportQuestionsDO> questionList = new ArrayList<>();
    public ArrayList<ReportImagesDO> imagesList = new ArrayList<>();

}
