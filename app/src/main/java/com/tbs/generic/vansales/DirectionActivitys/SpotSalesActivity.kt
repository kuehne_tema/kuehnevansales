package com.tbs.generic.pod.Activitys

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView
import com.tbs.generic.vansales.Activitys.ActiveDeliveryActivity
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Activitys.SpotSalesCustomerActivity
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils



class SpotSalesActivity : BaseActivity() {
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200


    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.spotsales_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        initializeControls()

    }


    override fun initializeControls() {

        tvScreenTitle.text = getString(R.string.spot_sales)
        var  llDelivery = findViewById<View>(R.id.llDelivery) as LinearLayout
        var  llActiveDelivery = findViewById<View>(R.id.llActiveDelivery) as LinearLayout

        llDelivery.setOnClickListener {
            val vehicleCheckOutDo = StorageManager.getInstance(this@SpotSalesActivity).getVehicleCheckOutData(this@SpotSalesActivity)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this@SpotSalesActivity).getVehicleCheckInData(this@SpotSalesActivity)
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                    var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                    if (shipmentId == null || shipmentId.equals("")) {
                        val intent = Intent(this@SpotSalesActivity, SpotSalesCustomerActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                    } else {
                        showToast("Please process the current schedule " + shipmentId + " shipment")
                    }
                } else {
                    showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                }
            } else {
                showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
            }
        }
        llActiveDelivery.setOnClickListener {
            val vehicleCheckOutDo = StorageManager.getInstance(this@SpotSalesActivity).getVehicleCheckOutData(this@SpotSalesActivity)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this@SpotSalesActivity).getVehicleCheckInData(this@SpotSalesActivity)
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                    var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                    if (shipmentId == null || shipmentId.equals("")) {
                        val customerDo = StorageManager.getInstance(this@SpotSalesActivity).getCurrentSpotSalesCustomer(this@SpotSalesActivity)
                        if (!customerDo.customer.equals("")) {
                            val intent = Intent(this@SpotSalesActivity, ActiveDeliveryActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                        }else{
                            showAppCompatAlert("", "No active delivery", "OK", "", "", false)

                        }
                    } else {
                        showAppCompatAlert("", "Please process the current schedule " + shipmentId + " shipment", "OK", "", "FAILURE", false)
                    }

                } else {
                    showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
                }
            } else {
                showAppCompatAlert("", "You have arrived for CheckOut, you cannot access at this time", "OK", "", "", false)
            }
        }


    }

    override fun onResume() {
        super.onResume()
    }


}