package com.tbs.generic.vansales.prints;

import java.io.Serializable;

public class PrintConfigDo implements Serializable {

    public String printerID = "";
    public String printerName = "";
    public String logoBase64 = "";
    public String base64Footer = "";
    public String jsonCmdAttribStr = "";
    public String signatureBase64 = "";
    public Object printData;
    public int printFrom;
}
