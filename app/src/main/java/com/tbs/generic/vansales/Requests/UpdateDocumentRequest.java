package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.Model.UpdateDocumentDO;
import com.tbs.generic.vansales.Model.ValidateDeliveryDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class UpdateDocumentRequest extends AsyncTask<String, Void, Boolean> {


    private Context mContext;
    private ValidateDeliveryDO validateDeliveryDO;
    private String id, notes, lattitude, longitude, address;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String message = "";

    public UpdateDocumentRequest(String note, String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
        this.notes = note;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, ValidateDeliveryDO validateDeliveryDO, String msg);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);

        UpdateDocumentDO updateDocumentDO = StorageManager.getInstance(mContext).getUpdateData(mContext);

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_XDOCUNUM", id);
            jsonObject.put("I_XNOTES", notes);
            if(!notes.isEmpty()){
                jsonObject.put("I_XNOTESFLG", 2);

            }else {
                jsonObject.put("I_XNOTESFLG", 1);

            }
            if(updateDocumentDO.getSignatureEncode().isEmpty()){
                jsonObject.put("I_XSIGNAFLG", 1);

            }else {
                jsonObject.put("I_XSIGNAFLG", 2);

            }

            jsonObject.put("I_XNOOFIMGES", updateDocumentDO.getCapturedImagesListBulk().size());

            jsonObject.put("I_XSIGNATURE", updateDocumentDO.getSignatureEncode());
            JSONArray jsonArray = new JSONArray();
            if (updateDocumentDO.getCapturedImagesListBulk() != null && updateDocumentDO.getCapturedImagesListBulk().size() > 0) {
                for (int i = 0; i < updateDocumentDO.getCapturedImagesListBulk().size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_XIMAGES", updateDocumentDO.getCapturedImagesListBulk().get(i));

                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.UPDATE_DOCUMENT, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("validated xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();

            validateDeliveryDO = new ValidateDeliveryDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {
                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            validateDeliveryDO.flag = Integer.parseInt(text);

                        } else if (attribute.equalsIgnoreCase("O_XMESSAGE")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.message = text;
                            }

                        }

                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

//         ProgressTask.getInstance().showProgress(mContext, false, "Validating the Delivery...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
//        ProgressTask.getInstance().closeProgress();

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, validateDeliveryDO, ServiceURLS.message);
        }
        ((BaseActivity) mContext).hideLoader();

    }
}