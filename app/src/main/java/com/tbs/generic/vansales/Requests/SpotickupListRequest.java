package com.tbs.generic.vansales.Requests;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.alert.rajdialogs.ProgressDialog;
import com.tbs.generic.vansales.Model.PickUpDo;
import com.tbs.generic.vansales.Model.SpotPickUpDo;
import com.tbs.generic.vansales.Model.SpotPickupMainDo;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by Vijay Dhas on 26/05/16.
 */
public class SpotickupListRequest extends AsyncTask<String, Void, Boolean> {
    SpotPickUpDo spotPickUpDO;
    SpotPickupMainDo spotPickupMainDo;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    Context mContext;
    String id;
    private ProgressDialog progressDialog;


    public SpotickupListRequest(Context mContext) {

        this.mContext = mContext;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SpotPickupMainDo spotPickupMainDo);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String sequence = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_YFCY", "" + sequence);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.SPOT_PICKUPSLIST, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("Delivery List xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            spotPickupMainDo  = new SpotPickupMainDo();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        spotPickupMainDo.spotPickUpDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        spotPickUpDO = new SpotPickUpDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YPRHFCY")) {
                            if (text.length() > 0) {
                                spotPickUpDO.site = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YPTHNUM")) {
                            if (text.length() > 0) {
                                spotPickUpDO.receiptNumber = text;
                            }
                        }   else if (attribute.equalsIgnoreCase("O_YXBPAADD")) {
                            if (text.length() > 0) {

                                spotPickUpDO.address = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YXBPSNUM")) {
                            if (text.length() > 0) {
                                spotPickUpDO.customer = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YXBPSNAME")) {
                            if (text.length() > 0) {

                                spotPickUpDO.description = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YLONG")) {
                            if (text.length() > 0) {

                                spotPickUpDO.lattitude = Double.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YLAT")) {
                            if (text.length() > 0) {

                                spotPickUpDO.longitude = Double.valueOf(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YPTHSPOTDIS")) {
                            if (text.length() > 0) {

                                spotPickupMainDo.distance = Integer.valueOf(text);
                            }
                        }

                        text = "";
                    }
                    if (endTag.equalsIgnoreCase("LIN")) {
                        spotPickupMainDo.spotPickUpDos.add(spotPickUpDO);
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            Log.e(this.getClass().getCanonicalName(), "parseXML() : " + e.getMessage());

            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, spotPickupMainDo);
        }
    }
}