package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.tbs.generic.vansales.Adapters.DamageScheduledProductsAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryDO
import com.tbs.generic.vansales.Model.ReasonMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.util.*


//
class DamageCylindersActivity : BaseActivity() {


    private var loadStockDOs: ArrayList<ActiveDeliveryDO>? = ArrayList()
    private var shipmentProductsList: ArrayList<ActiveDeliveryDO>? = ArrayList()

    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnRequestApproval: Button
    lateinit var btnConfirm: Button
    lateinit var tvNoDataFound: TextView
    lateinit var tvApprovalStatus: TextView
    private var shipmentType: String = ""
    var status = ""
    var customerId = ""
    var isProductExisted = false

    var damageId = 0
    lateinit var dialog: Dialog
    private var shipmentId: String = ""
    private var loadStockAdapter: DamageScheduledProductsAdapter? = null
    private var reasonMainDO: ReasonMainDO = ReasonMainDO()

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.scheduled_capture_delivery, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener {

            if (loadStockDOs != null && !loadStockDOs!!.isEmpty() && loadStockDOs!!.size > 0) {
                if (isProductExisted) {
                    StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity)
                    var intent = Intent()
                    var args = Bundle()
                    intent.putExtra("BUNDLE", args)
                    setResult(56, intent)
                    finish()
                } else {
                    showToast(getString(R.string.please_confirm))

                }
            } else {
                StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity)
                var intent = Intent()
                var args = Bundle()
                intent.putExtra("BUNDLE", args)
                setResult(56, intent)
                finish()
            }


        }

        initializeControls()
        shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        if (intent.hasExtra("DAMAGE")) {
            damageId = intent.extras!!.getInt("DAMAGE")
        }
        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras?.getString("CustomerId")!!
        }
        ivAdd.visibility = View.VISIBLE
        tvScreenTitle.text = getString(R.string.damaged_cylinders)


        btnConfirm.text = getString(R.string.confirm)
        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        btnConfirm.isClickable = false
        btnConfirm.isEnabled = false


        ivAdd.setOnClickListener{
            Util.preventTwoClick(it)
            val intent = Intent(this, DamageAddProductActivity::class.java)
            // need condition
            intent.putExtra("FROM", "SPOTSALES")
            startActivityForResult(intent, 1)
        }

        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)

            if (loadStockDOs != null && !loadStockDOs!!.isEmpty() && loadStockDOs!!.size > 0) {
                var isProductExisted = false

                for (k in loadStockDOs!!.indices) {
                    if (loadStockDOs!!.get(k).reasonId == 0) {
                        isProductExisted = true
                        break
                    }
                }

                if (isProductExisted) {
                    showToast(getString(R.string.please_select_reason))

                } else {
                    updateDamageCylinders()

                }


            } else {
                showToast(getString(R.string.no_items_found))
            }
        }


    }


    private fun updateDamageCylinders() {
        if (Util.isNetworkAvailable(this)) {

            val request = UpdateDamageCylindersRequest(shipmentId, loadStockDOs, this@DamageCylindersActivity)
            request.setOnResultListener { isError, approveDO ->
                hideLoader()

                if (approveDO != null) {
                    if (isError) {
                        showAppCompatAlert("", getString(R.string.please_try_again), getString(R.string.ok), getString(R.string.cancel), "", false)
                        isProductExisted = true

                    } else {
                        if (approveDO.flag == 1) {
                            showToast(getString(R.string.updated_successfully))
                            setResult(98, intent)
                            preferenceUtils.saveString(PreferenceUtils.DAMAGE, "DAMAGE")

                            finish()
                        } else {
                            isProductExisted = true
                            showAppCompatAlert("", getString(R.string.please_try_again), getString(R.string.ok), getString(R.string.cancel), "", false)

                        }
                    }
                }
            }
            request.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }

    }

    lateinit var loadStockDO: ActiveDeliveryDO

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            tvApprovalStatus.visibility = View.GONE
//            loadStockDOs = data!!.getSerializableExtra("AddedProducts") as ArrayList<ActiveDeliveryDO>
            loadStockDOs = StorageManager.getInstance(this@DamageCylindersActivity).getDamageDeliveryItems(this@DamageCylindersActivity)
            loadStockAdapter = DamageScheduledProductsAdapter(this, loadStockDOs, "", "Damage")
            recycleview.adapter = loadStockAdapter
            if (loadStockDOs!!.size > 0) {
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                btnConfirm.isClickable = true
                btnConfirm.isEnabled = true
                recycleview.visibility = VISIBLE
                tvNoDataFound.visibility = GONE
            }
//
//                    var isProductExisted = false;
//                    for (i in loadStockDOs!!.indices) {
//                        for (k in shipmentProductsList!!.indices) {
//                            if ( loadStockDOs!!.get(i).product.equals(shipmentProductsList!!.get(k).product, true) ) {
//                                shipmentProductsList!!.get(k).orderedQuantity = loadStockDOs!!.get(i).orderedQuantity
//                                isProductExisted = true
//                                shipmentProductsList!!.add(loadStockDOs!!.get(i))
//
//                                break
//                            }
//                        }
//
//                        if (isProductExisted) {
//                            isProductExisted = false
//                            continue
//                        } else {
//                            shipmentProductsList!!.add(loadStockDOs!!.get(i))
//                        }
//                    }
//                        loadStockAdapter = DamageScheduledProductsAdapter(this, shipmentProductsList, "", "Damage");
//                        recycleview.adapter = loadStockAdapter;
//
//            }
        }
    }


    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        btnRequestApproval = findViewById<View>(R.id.btnRequestApproval) as Button
        btnConfirm = findViewById<View>(R.id.btnConfirm) as Button
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        tvApprovalStatus = findViewById<TextView>(R.id.tvApprovalStatus)
        tvApprovalStatus.visibility = GONE
        btnRequestApproval.visibility = GONE

        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)

    }

    override fun onBackPressed() {

        if (loadStockDOs != null && !loadStockDOs!!.isEmpty() && loadStockDOs!!.size > 0) {
            if (isProductExisted) {
                StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity)
                var intent = Intent()
                var args = Bundle()
                intent.putExtra("BUNDLE", args)
                setResult(56, intent)
                finish()
            } else {
                showToast(getString(R.string.please_confirm))

            }
        } else {
            StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity)
            var intent = Intent()
            var args = Bundle()
            intent.putExtra("BUNDLE", args)
            setResult(56, intent)
            finish()
        }


    }

    override fun onDestroy() {
        StorageManager.getInstance(this@DamageCylindersActivity).deleteDamageDeliveryItems(this@DamageCylindersActivity)

        super.onDestroy()

    }

}