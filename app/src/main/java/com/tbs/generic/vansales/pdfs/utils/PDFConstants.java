package com.tbs.generic.vansales.pdfs.utils;

/*
 * Created by developer on 3/2/19.
 */
public interface PDFConstants {

    String ISO_LOGO_NAME = "iso_logo.png";
    String AUTHOR = "TBS";
    String CREATOR = "Venu Appasani";

    //CYLDeliveryNotePDF Constants
    String  CYL_DELIVERY_NOTE_PDF_NAME = "Document.pdf";
    String BG_INVOICE_PDF_NAME = "BGInvoice.pdf";
    String CYLINDER_ISS_REC_PDF_NAME = "CylinderIssRec.pdf";
    String BULK_DELIVERY_NOTE_PDF_NAME = "BulkDeliveryNote.pdf";
    String TAQAT_INVOICE_PDF_NAME = "TaqatInvoice.pdf";
    String RECEIPT_PDF_NAME = "TbsReceipt.pdf";

    String CYLINDER_ISS_REC = "CylinderIssRec";
    String HEADER_2 = "Delivery Note";
    String CYL_NAME = "CYL Delivery Note";
    String D_R_NO = "D.N. No";
    String DELIVERY_TO = "Delivered To";
    String ADDRESS = "Address";
    String DATE = "Date";
    String TIME = "Time";
    String USER_NAME = "User Name";
    String USER_ID = "User ID";
    String SR_NO = "Sr. No.";
    String ITEM = "Item";
    String QUANTITY = "Quantity";
    String COLOUMN = ":";

    //BG Invoice
    String HEADER_TEXT_BG_INVOICE = "Commercial and Tax Invoice";
    String INVOICE_NUMBER = "Invoice Number";
    String CUSTOMER_NAME = "Customer Name";
    String CUSTOMER_TRN = "Customer TRN";
    String CUSTOMER_ADDRESS = "Customer Address";
    String BG_INVOCE_NAME = "BG Invoice";
    String PRE_INVOICE_NUMBER = "PreInvoice Number";

    //Taqat Invoice
    String TAQAT_INVOCE_NAME = "Taqat Invoice";
    String OPENING_READING = "Opening \n Reading";
    String ENDING_READING = "Ending \n Reading";
    String NET_QUANTITY = "Net Quantity";

    //RECEPIT
    String RECEICPT_NAME = "Receipt";
    String RECEIPT_NO = "Receipt No.";
    String RECEIVED_FROM = "Received From";


    //CylinderIssRec


    //Common fields
    String SUPPLIER_NAME = "Supplier Name";
    String SUPPLIER_TRN = "Supplier TRN";
    String REGISTERED_ADDRESS = "Registered Address";
    String PARTICULARS = "Particulars";
    String QTY = "QTY";
    String UNIT_PRICE = "Unit Price (AED)";
    String AMOUNT = "Amount (AED)";
}
