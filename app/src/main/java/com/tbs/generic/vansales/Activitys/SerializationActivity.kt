package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tbs.generic.vansales.Adapters.InvoiceAdapter
import com.tbs.generic.vansales.Adapters.SerialSelectionAdapter
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.CreateLotRequest
import com.tbs.generic.vansales.Requests.SaveSerialsRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.activity_equipserial_number_allocation.*
import kotlinx.android.synthetic.main.activity_serial_number_allocation.*
import kotlinx.android.synthetic.main.activity_serial_number_allocation.btn_save
import kotlinx.android.synthetic.main.activity_serial_number_allocation.etSearchh
import kotlinx.android.synthetic.main.activity_serial_number_allocation.ivClearSearchh
import kotlinx.android.synthetic.main.activity_serial_number_allocation.llSearchh
import kotlinx.android.synthetic.main.activity_serial_number_allocation.tvBookedQty
import kotlinx.android.synthetic.main.activity_serial_number_allocation.tv_product
import kotlinx.android.synthetic.main.configuration_layout.*


class SerializationActivity : BaseActivity() {

    lateinit var invoiceAdapter: InvoiceAdapter
    var recieptDos: ArrayList<ActiveDeliveryDO> = ArrayList()
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var serialLotDO: ActiveDeliveryDO = ActiveDeliveryDO()
    lateinit var view: LinearLayout
    lateinit var btnAddSerialLot: Button
    lateinit var btnConfirm: Button
    lateinit var selectDOS: ArrayList<SerialListDO>
    lateinit var adapter: SerialSelectionAdapter

    var fromId = 0

    lateinit var activeDeliveryDO: TrailerSelectionDO
    lateinit var stockItemDO: StockItemDo

     var position: Int = 0
    var stock: Int = 0

    private var initAL: String? = null // keep starting AN
    private var BASE: Int = 0 // base of numberset is 62 so, addition = (anynumber %
    // BASE) and spare = (anynumber / 62)

    override fun onResume() {
        super.onResume()
//        selectInvoiceList()
    }


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.activity_equipserial_number_allocation, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()
    }

    override fun initializeControls() {
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.rcv_stock_selection)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        recycleview.layoutManager = linearLayoutManager
        this.initAL = initAL
        if (intent.hasExtra("STOCK")) {
            stock = intent.extras!!.getInt("STOCK")
        }




        if(stock==2){
            tv_title.setText(resources.getString(R.string.serial_stock))

            if (intent.hasExtra("PRODUCTDO")) {
                stockItemDO = intent.extras!!.getSerializable("PRODUCTDO") as StockItemDo
            }
            setStockData()
            llSearchh.visibility=View.GONE
            btn_save.visibility=View.GONE
            tv_product.setText(stockItemDO.productName+" - "+stockItemDO.productDescription)
            qtyLL.visibility=View.GONE
        }else{
            tv_title.setText(resources.getString(R.string.serialallocation))
            if (intent.hasExtra("PRODUCTDO")) {
                activeDeliveryDO = intent.extras!!.getSerializable("PRODUCTDO") as TrailerSelectionDO
            }

            if (intent.hasExtra("POSITION")) {
                position = intent.extras!!.getInt("POSITION")
            }
            tv_product.setText(activeDeliveryDO.trailer+" - "+activeDeliveryDO.trailerDescription)
            tvBookedQty.setText(""+activeDeliveryDO.quantity)
            llSearchh.visibility = View.VISIBLE
            btn_save.visibility=View.VISIBLE
            setData()

        }

        btn_save.setOnClickListener {
            if(adapter!=null&&adapter.selectedTrailerDOs.size==activeDeliveryDO.quantity){
                saveSerials()
            }else{
                showToast(resources.getString(R.string.qty_notmatching))
            }

        }



        ivClearSearchh.setOnClickListener {
            Util.preventTwoClick(it)
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            etSearchh.requestFocus()
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
            llSearchh.visibility = View.VISIBLE

        }


        etSearchh.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if (etSearchh.text.toString().equals("", true)) {
                    if (selectDOS != null && selectDOS!!.size > 0) {
//                        for (i in selectDOS!!.indices) {
//                            selectDOS!!.get(i).ischecked=false
//                        }
                        adapter = SerialSelectionAdapter(stockItemDO,activeDeliveryDO,this@SerializationActivity, selectDOS)
                        recycleview.adapter = adapter
                        tvNoDataFound.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    } else {
                        tvNoDataFound.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                } else if (etSearchh.text.toString().length > 1) {
                    filter(etSearchh.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun setData() {

        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = EquipmentSerialListRequest(activeDeliveryDO, this@SerializationActivity)
            driverListRequest.setOnResultListener { isError, serialDOS ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    btn_save.visibility = View.GONE
                } else {
                    if (serialDOS != null && serialDOS.size > 0) {
                        selectDOS = serialDOS
                        stockItemDO= StockItemDo()
                        adapter = SerialSelectionAdapter(stockItemDO,activeDeliveryDO,this@SerializationActivity, serialDOS)
                        recycleview.adapter = adapter
                        btn_save.visibility = View.VISIBLE

                    } else {
                        tvNoDataFound.visibility = View.VISIBLE
                        btn_save.visibility = View.GONE

                        recycleview.visibility = View.GONE
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }
    }
    private fun setStockData() {

        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = StockSerialListRequest(stockItemDO, this@SerializationActivity)
            driverListRequest.setOnResultListener { isError, serialDOS ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    btn_save.visibility = View.GONE
                } else {
                    if (serialDOS != null && serialDOS.size > 0) {
//                        selectDOS = serialDOS
                        activeDeliveryDO= TrailerSelectionDO()
                        adapter = SerialSelectionAdapter(stockItemDO,activeDeliveryDO,this@SerializationActivity, serialDOS)
                        recycleview.adapter = adapter
                        btn_save.visibility=View.GONE

                    } else {
                        tvNoDataFound.visibility = View.VISIBLE
                        btn_save.visibility = View.GONE

                        recycleview.visibility = View.GONE
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }
    }
    private fun filter(filtered: String): ArrayList<SerialListDO> {
        val customerDOs = ArrayList<SerialListDO>()
        for (i in selectDOS!!.indices) {
            if (selectDOS!!.get(i).serialDO.contains(filtered, true)
                    || selectDOS!!.get(i).serialDO.contains(filtered, true)) {
                customerDOs.add(selectDOS!!.get(i))
            }
        }
        if (customerDOs.size > 0) {
            adapter.refreshAdapter(customerDOs)
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        } else {
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }

    override fun onBackPressed() {
        if (!svSearch.isIconified) {
            svSearch.isIconified = true
            return
        }
        super.onBackPressed()
    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            val siteListRequest = CreateLotRequest(recieptDos, this@SerializationActivity)
            siteListRequest.setOnResultListener { isError, approveDO, msg ->
                hideLoader()

                if (isError) {
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                    }
                } else {
                    if (approveDO.flag == 2) {
                        val intent = Intent()
                        intent.putExtra("RecieptDOS", recieptDos)
                        setResult(10, intent)
                        finish()
                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)
                    }

                }

            }
            siteListRequest.execute()

        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {

            }

    }

    private fun saveSerials() {

        if (Util.isNetworkAvailable(this)) {
            showLoader()
            val request = SaveSerialsRequest(
                    this,
                    adapter.selectedTrailerDOs,activeDeliveryDO
            )
            request.setOnResultListener { isError, contarctMainDO ->
                hideLoader()
                if (isError) {
                    showToast(resources.getString(R.string.server_error));

                } else {
                    if (contarctMainDO != null) {
                        if (contarctMainDO.flag == 20) {
                            var equipDos = StorageManager.getInstance(this).getSerialEquipmentSelectionDO(this)
//                            val itemIndex: Int = equipDos.indexOf(ac)

                                activeDeliveryDO.isUpdated=2
                                equipDos.set(position, activeDeliveryDO)

                            StorageManager.getInstance(this).saveSerialEquipmentSelectionDO(this, equipDos)

                            showToast(contarctMainDO.successFlag)
                            finish()
//                           modify(contarctMainDO.successFlag)
                        } else {
                            showToast(resources.getString(R.string.server_error));

                        }

                    } else {
                        showToast(resources.getString(R.string.server_error));

                    }
                }

            }
            request.execute()
        } else {
            showToast(resources.getString(R.string.internet_connection));
        }
    }
//    private fun modify(success:String) {
//        if (Util.isNetworkAvailable(this)) {
//            val siteListRequest = SerialModifyRequest( activeDeliveryDO.loandelivery, activeDeliveryDO, this@SerializationActivity)
//            siteListRequest.setOnResultListener { isError, approveDO, msg ->
//                hideLoader()
//                if (isError) {
//                    if(msg.isNullOrEmpty()){
//                        showAlert(getString(R.string.server_error))
//                    }else{
//                        showAlert(msg)
//                    }
//
////                        Log.e("RequestAproval", "Response : " + isError)
//                } else {
//                    if(approveDO.flag==2){
//                        showToast(success);
//                        finish()
//                    }else{
//                        showAlert(getString(R.string.server_error))
//
//                    }
//
//
//                }
//
//            }
//            siteListRequest.execute()
//        } else {
//            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)
//
//        }
//
//
//    }
}