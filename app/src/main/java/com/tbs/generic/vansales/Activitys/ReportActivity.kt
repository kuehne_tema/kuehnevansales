package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.os.Bundle
import android.util.Base64
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.tbs.generic.vansales.Adapters.ReportImagesAdapter
import com.tbs.generic.vansales.Model.ReportMainDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ReportRequest
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.Constants
import com.tbs.generic.vansales.utils.Util
import com.tbs.rentalmanagement.inspection.adapters.ReportInspectionItemAdapter
import kotlinx.android.synthetic.main.activity_report.*
import kotlinx.android.synthetic.main.include_inspection_info.*

import java.util.*


class ReportActivity : BaseActivity() {
    private var type: Int = 0
    lateinit var contract: String
     var preparation: String="abc"
    var lineNumber =0
    private var serialNumber: String = ""

    var site = ""
    var adapter =
        ReportInspectionItemAdapter(this, ArrayList())

    override fun initialize() {
        val llCategories = layoutInflater.inflate(R.layout.activity_report, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvScreenTitle.text = getString(R.string.activity_report)

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        initializeControls()

    }

    override fun initializeControls() {
        tvScreenTitle.setText("Report")

        if (intent.hasExtra(Constants.SCREEN_TYPE)) {
            type = intent.extras!!.getInt(Constants.SCREEN_TYPE)!!
        }
        if (intent.hasExtra(Constants.CONTRACT_NUMBER)) {
            contract = intent.extras!!.getString(Constants.CONTRACT_NUMBER)!!
        }

        if (intent.hasExtra(Constants.LINE_NUMBER)) {
            lineNumber = intent.extras!!.getInt(Constants.LINE_NUMBER)!!
        }
        if (intent.hasExtra(Constants.SERIAL_NUMBER)) {
            serialNumber = intent.extras!!.getString(Constants.SERIAL_NUMBER)!!
        }
        intitiateControls();

    }


    private fun intitiateControls() {
        btn_service_repair.visibility = View.GONE

        if (Util.isNetworkAvailable(this)) {
            val request = ReportRequest(
                this,
                type,
                contract,
                serialNumber,
                lineNumber
            )
            request.setOnResultListener { isError, reportMainDO ->
                if (isError) {
                    Util.showToast(this, resources.getString(R.string.server_error));
                    tvNoDataFound.visibility = View.VISIBLE
                } else {
                    if (reportMainDO != null) {
                        setDataToViews(reportMainDO)

                    } else {
                        lldetails.visibility = View.GONE
                        //Utility.showToast(this, resources.getString(R.string.no_data));
                        tvNoDataFound.visibility = View.VISIBLE

                    }


                }
            }
            request.execute()
        } else {
            Util.showToast(this, resources.getString(R.string.internet_connection));
        }

    }

    private fun setDataToViews(reportMainDO: ReportMainDo) {
        val font: Typeface = Typeface.createFromAsset(assets, "montserrat_regular.ttf")

        tv_contract.text = contract

        tv_equipment.text = reportMainDO.product + "\n" + reportMainDO.productDescription
        if(serialNumber.isEmpty()){
            llSerials.visibility=View.GONE
        }else{
            llSerials.visibility=View.VISIBLE
            tv_serialNumber.text = reportMainDO.serialNumber

        }


        if (reportMainDO.questionList.size > 0) {
            ll_rcv_repot.visibility = View.VISIBLE
            adapter = ReportInspectionItemAdapter(
                this,
                reportMainDO.questionList
            )
            rcv_report.adapter = adapter
        } else {
            ll_rcv_repot.visibility = View.GONE
        }

        if (reportMainDO.imagesList.size > 0) {
            rcv_images.visibility = View.VISIBLE
            val adapter = ReportImagesAdapter(
                this,
                reportMainDO.imagesList, StringListner { }
            )
            rcv_images.adapter = adapter
        } else {
            rcv_images.visibility = View.GONE
        }

        tv_generated_by.text = reportMainDO.user
        tv_generated_date.text = CalendarUtils.getFormatedDate(reportMainDO.date)
        tv_generated_time.text = reportMainDO.time

        tv_conducted_by.text = CalendarUtils.getStringOfHexadecilmal(reportMainDO.conductedBy)
        tv_conducted_by.typeface = font

        tv_conducted_date.text = CalendarUtils.getFormatedDate(reportMainDO.date)
        tv_conducted_time.text = reportMainDO.time

        tv_main_commnets.text = CalendarUtils.getStringOfHexadecilmal(reportMainDO.mainComment);


        val encodeByte = Base64.decode(reportMainDO.signature, Base64.DEFAULT)
        val bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)

        tv_signature.setImageBitmap(bitmap)



//        btn_email.setOnClickListener {
//
//            if (Util.isNetworkAvailable(this)) {
//                val request = EmailSentRequest(
//                    this,
//                    type,
//                    contract,
//                    preparation,
//                    lineNumber,
//                    reportMainDO.product,
//                    reportMainDO.bookingReference
//
//                )
//                request.setOnResultListener { isError, successDo ->
//                    if (isError) {
//                        Util.showToast(this, resources.getString(R.string.server_error));
//                        tvNoDataFound.visibility = View.VISIBLE
//                    } else {
//                        if (successDo != null && successDo.flag == 2) {
//
//                            YesOrNoDialogFragment().newInstance(
//                                "Success",
//                                successDo.message,
//                                ResultListner { `object`, isSuccess ->
//                                    finish()
//                                    InspectionListActivity.setRefresh = true
//                                },
//                                false
//                            ).show(supportFragmentManager, "YesOrNoDialogFragment")
//                        }
//
//
//                    }
//                }
//                request.execute()
//            } else {
//                Util.showToast(this, resources.getString(R.string.internet_connection));
//            }
//
//        }
        btn_close.setOnClickListener {

            val intent =
                Intent(this, PODActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
    }

}
