package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.InspectionDO;
import com.tbs.generic.vansales.Model.InspectionMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class GateInspectionRequest extends AsyncTask<String, Void, Boolean> {

    private InspectionMainDO inspectionMainDO;
    private InspectionDO inspectionDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;


    public GateInspectionRequest(Context mContext) {

        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, InspectionMainDO driverIdMainDO);

    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_XGATECL", "GX10CGCK");
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.GATE_CHECKLIST, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", attribute1 = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            inspectionMainDO = new InspectionMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        attribute1 = xpp.getAttributeValue(null, "ID");

                        if (attribute1.equalsIgnoreCase("GRP2")) {

                            inspectionMainDO.inspectionDOS = new ArrayList<>();
                        }

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        // String  attribute2 = xpp.getAttributeValue(null, "ID");
                        if (attribute1.equalsIgnoreCase("GRP2")) {

                            inspectionDO = new InspectionDO();
                        }
//                        if(attribute2.equalsIgnoreCase("GRP2")){
//                        }else {
                    }
//                    else if (startTag.equalsIgnoreCase("LIN")) {
//                            driverEmptyDO = new DriverEmptyDO();
//
//
//
//
//                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
//                        if (attribute.equalsIgnoreCase("I_YBPCNUM")) {
//                            customerDetailsDo.number(text);
//                            customerDetailsDos.add(customerDetailsDo);
//
//                        }else

                        if (attribute.equalsIgnoreCase("O_XQUESTION")) {
                            inspectionDO.question = text;


                        }

                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        if (attribute1.equalsIgnoreCase("GRP2")) {

                            inspectionMainDO.inspectionDOS.add(inspectionDO);
                        }

                    }


                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //  ((BaseActivity)mContext).showLoader();

    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, inspectionMainDO);
        }
    }
}